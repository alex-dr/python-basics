# Simple Python project

I created this project to learn the basics of Python. 

## Python variables

| Variable Type | Description                               | Example                                 |
|---------------|-------------------------------------------|-----------------------------------------|
| Integer       | Represents whole numbers                  | `my_integer = 10`                       |
| Float         | Represents real numbers                   | `my_float = 20.5`                       |
| String        | Represents sequences of characters        | `my_string = "Hello, World!"`           |
| Boolean       | Represents logical values (True or False) | `my_boolean = True`                     |
| List          | Ordered collection of items               | `my_list = [1, 2, 3, 4, 5]`             |
| Tuple         | Ordered collection of items (immutable)   | `my_tuple = (6, 7, 8, 9, 10)`           |
| Dictionary    | Unordered collection of key-value pairs   | `my_dict = {'name': 'John', 'age': 30}` |
| Set           | Unordered collection of unique items      | `my_set = {1, 2, 3}`                    |
| None          | Represents the absence of a value         | `my_none = None`                        |
| Custom Object | User-defined data structure               | `my_object = MyObject("Sample Object")` |

