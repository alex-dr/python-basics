def test_args_kwargs(*args, **kwargs):
    # *args allows the function to accept any number of positional arguments.
    # These arguments are accessed as a tuple named 'args' within the function.
    print("args:", args)  # Prints the positional arguments.

    # **kwargs allows the function to accept any number of keyword arguments.
    # These arguments are accessed as a dictionary named 'kwargs' within the function.
    print("kwargs:", kwargs)  # Prints the keyword arguments.


# Example usage of the function
def main():
    # Using *args to pass positional arguments
    # Here, 1, 2, 3 are passed as positional arguments.
    # They will be received by the function as a tuple (1, 2, 3).
    print("Using *args:")
    test_args_kwargs(1, 2, 3)

    # Using **kwargs to pass keyword arguments
    # Here, a=1, b=2, c=3 are passed as keyword arguments.
    # They will be received by the function as a dictionary {'a': 1, 'b': 2, 'c': 3}.
    print("\nUsing **kwargs:")
    test_args_kwargs(a=1, b=2, c=3)

    # Using both *args and **kwargs
    # Here, 1, 2 are positional arguments and a=3, b=4 are keyword arguments.
    # Positional arguments will form a tuple (1, 2), and keyword arguments will form a dictionary {'a': 3, 'b': 4}.
    print("\nUsing both *args and **kwargs:")
    test_args_kwargs(1, 2, a=3, b=4)


if __name__ == "__main__":
    main()
