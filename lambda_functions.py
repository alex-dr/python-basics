# Example 1: Basic use of a lambda function
# This lambda function takes one argument 'x' and returns 'x' multiplied by 2
double = lambda x: x * 2
print("Double of 5 is:", double(5))  # Outputs 10

# Example 2: Lambda function with multiple parameters
# This lambda function takes two arguments 'x' and 'y' and returns their sum
sum = lambda x, y: x + y
print("Sum of 3 and 4 is:", sum(3, 4))  # Outputs 7



# Example 3: Sorting a list of tuples using lambda
# In this example, we have a list of tuples, where each tuple consists of two elements (a number and a character).
list_of_tuples = [(1, 'z'), (2, 'y'), (3, 'x')]

# We want to sort this list, but not in the usual way (which would typically sort based on the first element of each tuple).
# Instead, we want to sort the list based on the second element of each tuple, i.e., the character.

# To achieve this, we use the 'sorted()' function, which is a built-in Python function that returns a new sorted list from the items in any iterable.
# The 'key' parameter of the 'sorted()' function is used to specify a function that serves as a key for the sort comparison.

# Here, we use a lambda function as the key function.
# This lambda function takes one argument 'x' (which will be each tuple in the list), and returns the  second element of the tuple (x[1]).
# The lambda function thus tells the 'sorted()' function to use the second element of each tuple as the basis for sorting.

sorted_list = sorted(list_of_tuples, key=lambda x: x[1])

# The result is a list of tuples sorted based on the second element (the character) in ascending order.
# Therefore, the tuple with 'x' comes first, followed by the one with 'y', and then 'z'.
print("Sorted list of tuples:", sorted_list)  # Outputs [(3, 'x'), (2, 'y'), (1, 'z')]

# This approach demonstrates the power of lambda functions to customize the behavior of functions like 'sorted()'.
# By using a lambda function, we can define an inline, one-time-use function that specifies the exact sorting criteria we need.


# We use the same lambda function as the key to sort based on the second element of each tuple.
# The addition of 'reverse=True' in the sorted() function call sorts the list in descending order.
sorted_list_descending = sorted(list_of_tuples, key=lambda x: x[1], reverse=True)

# The result is a list of tuples sorted based on the second element in descending order.
# Therefore, the tuple with 'z' comes first, followed by the one with 'y', and then 'x'.
print("Sorted list of tuples in descending order:", sorted_list_descending)  # Outputs [(1, 'z'), (2, 'y'), (3, 'x')]

# Example 4: Using lambda with map()
# The map function applies the lambda function to each element of the list
numbers = [1, 2, 3, 4]
squared = list(map(lambda x: x ** 2, numbers))
print("Squares of the list elements:", squared)  # Outputs [1, 4, 9, 16]

# Example 5: Using lambda with filter()
# The filter function uses the lambda function to filter elements of the list
even_numbers = list(filter(lambda x: x % 2 == 0, numbers))
print("Even numbers from the list:", even_numbers)  # Outputs [2, 4]


# Example 6: Creating higher-order functions with lambda
# Create a function that returns a lambda function
def power(n):
    return lambda x: x ** n


# Create an instance of this function
square = power(2)
print("Square of 3:", square(3))  # Outputs 9

# Example 7: Using lambda with reduce()
from functools import reduce

# The reduce function applies the lambda function to reduce the list to a single value
total = reduce(lambda x, y: x + y, numbers)
print("Sum of the list elements:", total)  # Outputs 10

# Example 8: Lambda function with no parameters
# A lambda function that takes no parameters and returns a constant value
always_five = lambda: 5
print("Always five:", always_five())  # Outputs 5
