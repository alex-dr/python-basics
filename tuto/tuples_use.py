# Creating tuples
coordinates = (10, 20)  # Parentheses create a tuple
empty_tuple = ()  # Empty tuple
single_element_tuple = (5,)  # Comma needed for single-element tuples
mixed_tuple = (1, "hello", 3.14)  # Tuples can contain different data types

# Accessing elements
print("X-coordinate:", coordinates[0])  # Access by index (starts from 0)
print("Third element:", mixed_tuple[2])

# Immutability: tuples cannot be changed after creation
try:
    coordinates[0] = 5  # This will raise an error
except TypeError as e:
    print(e)

# Using tuples for multiple assignment
x, y = coordinates  # Unpack values into separate variables
print("x:", x, "y:", y)


# Tuples as function arguments
def calculate_area(width, height):
    return width * height


area = calculate_area(*coordinates)  # Unpack tuple as arguments
print("Area:", area)


# Tuples as function return values
def get_user_info():
    name = input("Enter your name: ")
    age = int(input("Enter your age: "))
    return name, age  # Return a tuple with name and age


user_name, user_age = get_user_info()  # Unpack returned tuple
print("User name:", user_name, "User age:", user_age)

# Comparing tuples
tuple1 = (1, 2, 3)
tuple2 = (1, 2, 3)
tuple3 = (3, 2, 1)
print(tuple1 == tuple2)  # True
print(tuple1 != tuple3)  # True
print(tuple1 < tuple3)  # True (lexicographical comparison)

# Tuples as dictionary keys
person = {coordinates: "Alice"}  # Tuple can be used as a key
print(person[coordinates])
