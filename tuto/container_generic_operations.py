import copy

# List operations
my_list = [1, 2, 3]
my_list.append(4)  # Add an element
print("List after appending:", my_list)
my_list.remove(2)  # Remove an element
print("List after removing:", my_list)

# Tuple operations (accessing elements, tuples are immutable)
my_tuple = (1, 2, 3)
print("Element at index 1 in tuple:", my_tuple[1])

# Set operations
my_set = {1, 2, 3}
my_set.add(4)  # Add an element
print("Set after adding:", my_set)
my_set.discard(2)  # Remove an element
print("Set after discarding:", my_set)

# Dictionary operations
my_dict = {'a': 1, 'b': 2, 'c': 3}
my_dict['d'] = 4  # Add a new key-value pair
print("Dictionary after adding:", my_dict)
del my_dict['b']  # Delete a key-value pair
print("Dictionary after deletion:", my_dict)

# Length of container
print("Length of list:", len(my_list))
print("Length of tuple:", len(my_tuple))
print("Length of set:", len(my_set))
print("Length of dictionary:", len(my_dict))

# Iterating over containers
print("Iterating over list:")
for item in my_list:
    print(item)

print("Iterating over dictionary keys:")
for key in my_dict:
    print(key)

# Using enumerate
for index, value in enumerate(my_list):
    print(f"Index {index}: Value {value}")

# Using all and any
all_positive = all(x > 0 for x in my_list)
print("All elements are positive:", all_positive)

contains_even = any(x % 2 == 0 for x in my_list)
print("List contains an even number:", contains_even)

# Using zip
list1 = [1, 2, 3]
list2 = ['a', 'b', 'c']
# Create an iterator that pairs elements from both lists
zipped = zip(list1, list2)
print("Zipped lists:", list(zipped))

# Combining elements into tuples:
numbers = [1, 2, 3]
letters = ['a', 'b', 'c']
symbols = ['!', '@', '#']

# Combine elements from all three lists into tuples
combined_tuples = zip(numbers, letters, symbols)
print(list(combined_tuples))  # Output: [(1, 'a', '!'), (2, 'b', '@'), (3, 'c', '#')]

# Iterating over multiple iterables simultaneously:
names = ['Alice', 'Bob', 'Charlie']
ages = [25, 30, 35]

# Iterate over both lists together, accessing corresponding elements
for name, age in zip(names, ages):
    print(f"{name} is {age} years old.")  # Output: Alice is 25 years old., etc.

# Creating dictionaries from parallel lists:
keys = ['name', 'age', 'city']
values = ['John', 30, 'New York']

# Create a dictionary using zip()
my_dict = dict(zip(keys, values))
print(my_dict)  # Output: {'name': 'John', 'age': 30, 'city': 'New York'}

# Using sorted and reversed
unsorted_list = [3, 1, 4, 1, 5]
sorted_list = sorted(unsorted_list)
print("Sorted list:", sorted_list)

reversed_list = list(reversed(my_list))
print("Reversed list:", reversed_list)

# Using copy and deepcopy
shallow_copied_list = copy.copy(my_list)
nested_list = [1, [2, 3], [4, 5]]
deep_copied_list = copy.deepcopy(nested_list)
nested_list[1][0] = 'Changed'

print("Original list after modification:", nested_list)
print("Deep copied list:", deep_copied_list)
