import random

# random.random(): Random float between 0.0 and 1.0
print("Random float: ", random.random())

# random.randint(a, b): Random integer between a and b
print("Random integer between 1 and 10: ", random.randint(1, 10))

# random.randint(a, b): Random integer between a and b
print("Random integer between 1 and 10: ", random.randint(1, 10))

# random.uniform(a, b): Random float between a and b
print("Random float between 1 and 10: ", random.uniform(1, 10))

# random.randrange(start, stop, step): Random element from range(start, stop, step)
print("Random number from range 0 to 100, stepping by 5: ", random.randrange(0, 101, 5))

# random.choice(seq): Random element from a sequence
my_list = ['apple', 'banana', 'cherry']
print("Random choice from a list: ", random.choice(my_list))

# random.shuffle(seq): Shuffle a sequence in place
random.shuffle(my_list)
print("List after shuffle: ", my_list)

# random.sample(population, k): k length list of unique elements chosen from population
print("Sample of 2 from list: ", random.sample(my_list, 2))

# random.choices(population, weights=None, cum_weights=None, k=1): k sized list of elements chosen with replacement
print("Choices with weights from list: ", random.choices(my_list, weights=[10, 1, 1], k=2))

# random.getrandbits(k): Python integer with k random bits
print("16-bit random integer: ", random.getrandbits(16))

# random.seed(a=None, version=2): Initialize the random number generator
random.seed(1)
print("Random number with seed: ", random.random())  # This will always print the same number

