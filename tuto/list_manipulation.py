
# Python List Methods Examples

# Creating an empty list
my_list = []

# append() - Adds an element at the end of the list
my_list.append(1)
my_list.append(2)
my_list.append(3)
print("List after append:", my_list)

# extend() - Adds all elements of a list to another list
my_list.extend([4, 5])
print("List after extend:", my_list)

# insert() - Inserts an element at the specified position
my_list.insert(1, 'a')
print("List after insert:", my_list)

# remove() - Removes the first item with the specified value
my_list.remove('a')
print("List after remove:", my_list)

# pop() - Removes the element at the specified position
popped_element = my_list.pop(1)
print("List after pop:", my_list)
print("Popped element:", popped_element)

# clear() - Removes all elements from the list
my_list.clear()
print("List after clear:", my_list)

# Re-populating list for further methods
my_list = [3, 1, 4, 1, 5, 9, 2]

# index() - Returns the index of the first element with the specified value
index_of_four = my_list.index(4)
print("Index of 4:", index_of_four)

# count() - Returns the number of elements with the specified value
count_of_one = my_list.count(1)
print("Count of 1:", count_of_one)

# sort() - Sorts the list
my_list.sort()
print("List after sort:", my_list)

# reverse() - Reverses the order of the list
my_list.reverse()
print("List after reverse:", my_list)

# copy() - Returns a copy of the list
copied_list = my_list.copy()
print("Copied list:", copied_list)