class Book:
    # Static property (belongs to the class, not instances)
    total_books = 0

    def __init__(self, title, author, opt_value=None):
        # Instance properties (each instance has its own title and author)
        self.title = title
        self.author = author
        self.opt_value = opt_value or "default value"
        self.__private_property = "private property"
        self.none_property = None
        # Modifying the static property
        Book.total_books += 1

    def display_info(self):
        # Instance method (uses instance properties)
        return (
            f"Book: {self.title} by {self.author} with opt_value {self.opt_value}, "
            f"with private property {self.__private_property},"
            f"with none property {self.none_property}")

    @staticmethod
    def get_total_books():
        # Static method (doesn't depend on instance properties)
        return Book.total_books


# Creating instances of the Book class
book1 = Book("1984", "George Orwell")
book2 = Book("To Kill a Mockingbird", "Harper Lee")

# Calling instance method
print(book1.display_info())  # Output: Book: 1984 by George Orwell

# Calling static method
print(Book.get_total_books())  # Output: 2
