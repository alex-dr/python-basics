# Concatenation
first_name = "John"
last_name = "Doe"
full_name = first_name + " " + last_name
print("Full name:", full_name)

# Substrings (slicing)
quote = "To be or not to be, that is the question."
print("First 10 characters:", quote[:10])  # Includes characters from index 0 to 9
print("Characters from 17 to 30:", quote[17:31])

# Finding substrings
text = "Python is a versatile programming language."
print("Position of 'Python':", text.find("Python"))
print("Position of 'Java':", text.find("Java"))  # Returns -1 if not found

# Replacing substrings
modified_text = text.replace("Python", "JavaScript")
print("Modified text:", modified_text)

# Splitting strings
words = text.split()
print("Words in the text:", words)

# Joining strings
joined_string = "-".join(words)
print("Joined string:", joined_string)

# Case conversion
lowercase_text = text.lower()
uppercase_text = text.upper()
title_text = text.title()
print("Lowercase:", lowercase_text)
print("Uppercase:", uppercase_text)
print("Title case:", title_text)

# Stripping whitespace
text_with_spaces = "   Hello, world!   "
stripped_text = text_with_spaces.strip()
print("Stripped text:", stripped_text)

# convert list to string
my_list = ['apple', 'banana', 'cherry']
my_string = ', '.join(my_list)
print(my_string)  # Output: apple, banana, cherry

# convert list to string
my_list = ['apple', 'banana', 'cherry']
my_string = ''.join(my_list)
print(my_string)  # Output: applebananacherry
