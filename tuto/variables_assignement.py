import math
import time

# 1. Direct assignment using the equal sign (=)
age = 25
name = "Alice"
is_active = True

# 2. Assigning multiple variables in one line
x, y, z = 10, 20.5, "hello"

# 3. Assigning values from expressions
result = 5 * 7 + 3
squared = 4 ** 2

# 4. Assigning values from functions

random_number = math.sin(22)

# 5. Assigning values from user input
username = input("Enter your username: ")

# 6. Assigning values from files (example using a text file)
# Open the file "data.txt" in read mode ("r") and assign the file object to the variable "file"
with open("data/data.txt", "r") as file:
    # Read the entire contents of the file into the variable "data"
    data = file.read()
    print(data)

# Explanation of the "with" keyword:
# - It's a context manager for automatic resource management.
# - It ensures the file is closed properly, even if exceptions occur.
# - It defines a code block where the file object is accessible.
# - It improves code readability and prevents resource leaks.

# 7. Assigning values from external modules
current_time = time.time()

# 8. Assigning values conditionally (using if-else)
if age >= 18:
    is_adult = True
else:
    is_adult = False

# 9. Assigning values using loops (example using a for loop)
numbers = []
for i in range(5):
    numbers.append(i * 2)

# 10. Assigning values using list comprehension
squared_numbers = [x ** 2 for x in range(10)]

# 11. Multiple assignments
x, y, z = 1, 2, 3

# 12. Values swap
a = 5
b = 10
a, b = b, a  # Swap values using tuple unpacking

# 13. Unpacking of sequence in items
coordinates = (10, 20)
x, y = coordinates
print('x ; ', x)
print('y ; ', y)

# 14. Unpacking of sequence in list
numbers = [1, 2, 3, 4, 5]
first, *rest, last = numbers  # Assign first and last elements, rest to a list

print('first unpacked ; ', first)
print('rest unpacked ; ', rest)
print('last unpacked ; ', last)

# 15. Unpacking with a placeholder
first, *_, last = numbers  # Ignore middle elements using underscore
