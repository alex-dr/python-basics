# Basic usage: generating a sequence of numbers
for i in range(5):  # Generates numbers from 0 to 4
    print(i)

# Specifying start, stop, and step
for i in range(2, 8, 2):  # Generates even numbers from 2 to 6 (exclusive)
    print(i)

# Using range to create lists
numbers = list(range(10))  # Create a list containing numbers from 0 to 9
print(numbers)

# Using range with negative steps
for i in range(10, 0, -1):  # Generates numbers from 10 down to 1
    print(i)

# Using range in other contexts
total = sum(range(1, 6))  # Calculate the sum of numbers from 1 to 5
print("Sum:", total)

# Combining range with other functions
squared_numbers = [x**2 for x in range(1, 4)]  # Create a list of squares using a list comprehension
print("Squared numbers:", squared_numbers)
