# Import the Vehicle class from vehicle.py
from vehicle import Vehicle


# Define the Car class that inherits from the Vehicle class
class Car(Vehicle):
    def __init__(self, make, model, year, color='White'):
        # Calling the superclass constructor with the 'color' parameter but without 'wheels'
        super().__init__(make, model, year, color=color)

    def display_info(self):
        # Overriding the display_info method
        info = "Car overridden display info:\n"
        # Adding the original Vehicle info
        info += super().display_info()
        return info


# Example usage
car = Car("Tesla", "Model 3", 2021, "Red")
print(car.display_info())
