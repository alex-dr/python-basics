from vehicle import Vehicle


class Motorcycle(Vehicle):
    def __init__(self, make, model, year, color='Black'):
        # Calling the superclass constructor with default wheels set to 2 for motorcycles
        super().__init__(make, model, year, color=color, wheels=2)

    def display_info(self):
        # Overriding the display_info method
        info = "Motorcycle overridden display info:\n"
        # Adding the original Vehicle info
        info += super().display_info()
        return info


# Example usage
motorcycle = Motorcycle("Harley-Davidson", "Street 750", 2021)
print(motorcycle.display_info())
