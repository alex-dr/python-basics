class Vehicle:
    def __init__(self, make, model, year, color='White', wheels=4):
        # Mandatory attributes
        self.make = make
        self.model = model
        self.year = year

        # Optional attributes with default values
        self.color = color
        self.wheels = wheels

    def display_info(self):
        return (f"Make: {self.make}, Model: {self.model}, Year: {self.year}, "
                f"Color: {self.color}, Wheels: {self.wheels}")
