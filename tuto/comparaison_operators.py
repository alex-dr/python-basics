# Equality and inequality
number1 = 25
number2 = 30

print("Equal to:", number1 == number2)
print("Not equal to:", number1 != number2)

# Greater than and less than
print("Greater than:", number1 > number2)
print("Less than:", number1 < number2)

# Greater than or equal to and less than or equal to
print("Greater than or equal to:", number1 >= number2)
print("Less than or equal to:", number1 <= number2)

# Comparing strings
name1 = "Alice"
name2 = "Bob"

print("Alice is before Bob alphabetically:", name1 < name2)

# Chaining comparisons
age = 35
print("Age between 20 and 40:", 20 <= age <= 40)

# Using comparison operators in conditional statements
if number1 > number2:
    print("Number1 is greater than number2.")
else:
    print("Number1 is less than or equal to number2.")
