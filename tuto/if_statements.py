# Simple if statement
temperature = 25
if temperature > 20:
    print("It's a warm day!")

# [Image of flowchart for simple if statement]

# If-else statement
age = 18
if age >= 18:
    print("You are eligible to vote.")
else:
    print("You are not eligible to vote.")

# [Image of flowchart for if-else statement]

# Nested if statements
grade = 85
if grade >= 90:
    print("Excellent!")
else:
    if grade >= 80:
        print("Very good!")
    else:
        print("Good job!")

# [Image of flowchart for nested if statements]

# Elif statement
day_of_week = "Tuesday"
if day_of_week == "Monday":
    print("Start of the workweek!")
elif day_of_week == "Friday":
    print("Almost the weekend!")
else:
    print("It's a regular workday.")

# [Image of flowchart for elif statement]

# Using logical operators in conditions
has_ticket = True
is_member = False
if has_ticket or is_member:
    print("Welcome to the event!")

# [Image of flowchart for if statement with logical operator]
