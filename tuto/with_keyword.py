import threading

# Using 'with' for file operations

# 1. Reading from a file
with open('data/data.txt', 'r') as file:
    content = file.read()
    print(content)  # The file is automatically closed after this block

# 2. Writing to a file
with open('data/output.txt', 'w') as file:
    file.write('Hello, World! (written using file.write)')  # File is automatically closed after writing

# Using 'with' with other context managers

# 3. Working with a context manager for locking resources (threading)

lock = threading.Lock()
with lock:
    # Critical section of code
    print('Lock is held during this block')
    # Lock is released here


# 4. Using 'with' for managing database connections (with a hypothetical database module)
# from database_module import DatabaseConnection
# with DatabaseConnection('database_uri') as connection:
#     connection.execute('SELECT * FROM table')
#     # Connection is automatically closed after this block

# Note: Uncomment the above lines after replacing 'database_module' and 'database_uri'
# with the actual module and URI when working with a real database.

# 5. Using 'with' with custom context managers (user-defined)


class CustomContextManager:

    # The __enter__ method is called when entering the context managed by the 'with' statement.
    # It is where you can perform setup actions.
    def __enter__(self):
        print('Entering the context')
        # Whatever is returned by __enter__ can be assigned to a variable after the 'as' keyword in the 'with'
        # statement. Here, it returns the string 'Some Resource', which is then assigned to the variable 'resource'.
        return 'Some Resource'

    # The __exit__ method is called when exiting the context, even if an exception occurred.
    # It can be used to perform cleanup actions, handle exceptions, or do something with the returned value.
    # The arguments 'exc_type', 'exc_value', and 'traceback' are for exception handling.
    def __exit__(self, exc_type, exc_value, traceback):
        # Here, you might add code to handle exceptions, if any, or perform cleanup.
        print('Exiting the context')
        # If __exit__ returns False, any exception that occurred within the 'with' block will be re-raised.
        # Returning True would suppress the exception.


# Using the custom context manager
with CustomContextManager() as resource:
    # The code here is the body of the 'with' statement, where 'resource' now holds the value 'Some Resource'.
    print('Inside custom context manager block')
    # You can use the resource here as needed.
    print('Resource:', resource)
    # After this block is completed or if an exception occurs, the __exit__ method of CustomContextManager is called.

# This script demonstrates various use cases of the 'with' keyword in Python.
