def safe_divide(num1, num2):
    try:
        # Try block where we attempt the division
        if num2 == 0:
            # Raise an exception if num2 is zero
            raise ValueError("Cannot divide by zero")
        result = num1 / num2
        print(f"The result of division is {result}")
    except ValueError as ve:
        # Handle specific ValueError (division by zero in our case)
        print("Error:", ve)
    except Exception as e:
        # Handle any other exceptions
        print("Unexpected exception of type :", type(e), ' occurred. Message :\n', '\t', e)


# Test cases
safe_divide(10, 2)  # Normal division
safe_divide(5, 0)  # Division by zero, will raise ValueError
safe_divide('a', 2)  # Invalid type for division, will raise TypeError



def divide_with_finally(num1, num2):
    try:
        # Attempt the division operation
        result = num1 / num2
        return result
    except ZeroDivisionError:
        # Handle division by zero
        print("Cannot divide by zero.")
        return None
    finally:
        # This block will execute regardless of what happens in the try and except blocks
        print("Execution of divide_with_finally completed.")

# Test cases
print("Result:", divide_with_finally(10, 2))   # Normal division
print("Result:", divide_with_finally(5, 0))    # Division by zero

