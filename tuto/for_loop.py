# Iterating over a list of numbers
numbers = [1, 4, 2, 5, 3]
for number in numbers:
    print(number * 2)  # Print each number doubled

# Iterating over a string
name = "Alice"
for character in name:
    print(character.upper())  # Print each character in uppercase

# Iterating over a range of numbers
for i in range(5):  # Generates numbers from 0 to 4
    print("Iteration:", i)

# Iterating over a dictionary (keys only)
person = {"name": "Bob", "age": 30}
for key in person:
    print(key, ":", person[key])  # Print key-value pairs

# Iterating over a dictionary (keys and values)
for key, value in person.items():
    print(key, "=", value)  # Print key-value pairs in a different format

# Using the break statement to exit early
for i in range(10):
    if i == 5:
        break  # Exit the loop when i reaches 5
    print(i)

# Using the continue statement to skip to the next iteration
for i in range(10):
    if i % 2 == 0:
        continue  # Skip even numbers
    print(i)

# Using the else clause to execute code after the loop completes normally
for i in range(5):
    print(i)
else:
    print("Loop finished without break")
