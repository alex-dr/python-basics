# Using built-in functions
import math

print("Absolute value of -5 : ", abs(-5))
print("Square root of 25 :", math.sqrt(25))
print("Ceiling of 4.2 :", math.ceil(4.2))
print("Floor or 4.8 : ", math.floor(4.8))
print("Rounding of 4.5 : ", round(4.5))  # Rounds to nearest even number by default

# Basic arithmetic operations
number1 = 10
number2 = 5

print("Addition:", number1 + number2)
print("Subtraction:", number1 - number2)
print("Multiplication:", number1 * number2)
print("Division (floating-point result):", number1 / number2)
print("Division (integer quotient):", number1 // number2)  # Floor division
print("Modulus (remainder):", number1 % number2)
print("Exponentiation:", number1 ** number2)

# Using variables in calculations
price = 15.99
quantity = 3
total_cost = price * quantity
discount = 0.15  # 15% discount
discounted_price = price * (1 - discount)

print("Total cost:", total_cost)
print("Discounted price:", discounted_price)

# Order of operations (PEMDAS)
result = 2 + 3 * 4  # Multiplication before addition
print("Result (PEMDAS):", result)

# Grouping with parentheses
result = (2 + 3) * 4  # Addition within parentheses first
print("Result with parentheses:", result)

