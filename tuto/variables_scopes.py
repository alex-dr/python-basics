# Global variable
global_number = 10


def demo_function():
    # Local variable
    local_number = 5

    # Accessing the global variable inside the function
    print(f"Inside function, global number: {global_number}")

    # Accessing the local variable inside the function
    print(f"Inside function, local number: {local_number}")


# Calling the function
demo_function()

# Accessing the global variable outside the function
print(f"Outside function, global number: {global_number}")


def modify_global_variable():
    # Declare that we want to use the global variable
    global global_number

    # Modify the global variable
    global_number = 99
    print(f"Inside function, global number changed to: {global_number}")


# Calling the function
modify_global_variable()

# Trying to access the local variable outside the function (this will cause an error)
# Uncomment the line below to see the error
# print(f"Outside function, local number: {local_number}")
