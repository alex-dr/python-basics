# Integer
my_integer = 10

# Float
my_float = 20.5

# String
my_string = "Hello, World!"

# Boolean, notice the uppercase first letter
my_boolean = True

# List - an ordered collection of items
my_list = [1, 2, 3, 4, 5]

# Tuple - similar to a list but immutable
my_tuple = (6, 7, 8, 9, 10)

# Dictionary - a collection of key-value pairs
my_dict = {'name': 'John', 'age': 30}

# Set - an unordered collection of unique items
my_set = {1, 2, 3}

# Creating a frozenset, representing an immutable set
# using the frozenset() constructor
immutable_set = frozenset([1, 2, 3, 4, 5])

# Attempting to add an element will raise an AttributeError
# immutable_set.add(6)  # Uncommenting this line will cause an error

# Performing set operations
other_set = frozenset([4, 5, 6, 7])
union_set = immutable_set.union(other_set)
print("Union:", union_set)

# frozenset can be a key in a dictionary
dict_with_frozenset = {immutable_set: "value1", other_set: "value2"}
print("Dictionary:", dict_with_frozenset)

# None - represents the absence of a value
my_none = None


# Object - user-defined data structure
class MyObject:
    def __init__(self, name):
        self.name = name


my_object = MyObject("Sample Object")

# Displaying the types
types = {
    "Integer": my_integer,
    "Float": my_float,
    "String": my_string,
    "Boolean": my_boolean,
    "List": my_list,
    "Tuple": my_tuple,
    "Dictionary": my_dict,
    "Set": my_set,
    "None": my_none,
    "Object": my_object
}

# Creating complex numbers
# 'complex_num1' is a complex number with a real part 3 and an imaginary part 4
complex_num1 = 3 + 4j
# 'complex_num2' is a complex number with a real part 1 and an imaginary part -2
complex_num2 = 1 - 2j

# Accessing real and imaginary parts
real_part = complex_num1.real  # 3.0
imaginary_part = complex_num1.imag  # 4.0

# Basic arithmetic operations with complex numbers
# Addition: Adding the real parts and the imaginary parts separately
# (3 + 4j) + (1 - 2j) = (3 + 1) + (4j - 2j) = 4 + 2j
addition = complex_num1 + complex_num2

# Subtraction: Subtracting the real parts and the imaginary parts separately
# (3 + 4j) - (1 - 2j) = (3 - 1) + (4j - (-2j)) = 2 + 6j
subtraction = complex_num1 - complex_num2

# Multiplication: Distributive property is applied (like FOIL method in algebra)
# (3 + 4j) * (1 - 2j) = 3*1 + 3*(-2j) + 4j*1 + 4j*(-2j)
# = 3 - 6j + 4j - 8j^2 (remembering that j^2 = -1)
# = 3 - 2j - 8*(-1) = 3 - 2j + 8 = 11 - 2j
multiplication = complex_num1 * complex_num2

# Division: Dividing complex numbers involves multiplying by the conjugate
# (3 + 4j) / (1 - 2j)
# To divide, multiply the numerator and denominator by the conjugate of the denominator
# (3 + 4j) * (1 + 2j) / ((1 - 2j)*(1 + 2j))
# The conjugate of (1 - 2j) is (1 + 2j)
# The denominator becomes (1 - 2j)*(1 + 2j) = 1 - 4j^2 = 1 - 4*(-1) = 1 + 4 = 5
# The numerator becomes (3 + 4j)*(1 + 2j) = 3 + 6j + 4j + 8j^2 = 3 + 10j - 8 = -5 + 10j
# So, (-5 + 10j) / 5 = -1 + 2j
division = complex_num1 / complex_num2

# Conjugate of a complex number
# The conjugate of a complex number changes the sign of the imaginary part.
# For complex_num1, which is 3 + 4j, the conjugate would be 3 - 4j.
# This means if the imaginary part is positive, it becomes negative, and vice versa.
# The real part of the number remains unchanged.
conjugate = complex_num1.conjugate()  # (3-4j)

print('complex_num1 ', complex_num1)
print('complex_num1 ', complex_num2)
print('addition ', addition)
print('multiplication ', multiplication)
