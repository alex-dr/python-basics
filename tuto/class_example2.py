class Person:
    """Represents a person with a controlled age attribute."""

    def __init__(self, age):
        """Initializes a Person object with the given age.

        Args:
            age (int): The initial age of the person.

        Raises:
            ValueError: If the age is negative.
        """
        self._age = age  # Store the age internally, prefixed with an underscore for convention

    @property
    def age(self):
        """Gets the person's age.

        Returns:
            int: The person's current age.
        """
        return self._age

    @age.setter
    def age(self, new_age):
        """Sets the person's age, ensuring it's non-negative.

        Args:
            new_age (int): The new age to set.

        Raises:
            ValueError: If the new age is negative.
        """
        if new_age < 0:
            raise ValueError("Age cannot be negative")  # Enforce age validation
        self._age = new_age

# Usage example:
person = Person(25)  # Create a Person object with an initial age of 25
print(person.age)  # Access the age property (calls the getter method)

try:
    person.age = -10  # Attempt to set a negative age (calls the setter method)
except ValueError as e:
    print(e)  # Handle the error if validation fails
