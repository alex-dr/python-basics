# Original list for demonstration
"""
When you make a shallow copy of a list, you are copying the list object itself, but not its contents.
This means that the new list object points to the same elements as the original list object.
Nevertheless, if in one of the two lists you modify an immutable element (e.g. a string),
 the change will not be reflected in the other list.
This happens because immutable elements cannot be modified in-place, so a new object is created instead.

Immutable Types in Python

 int: Immutable integers
 float: Immutable floating point numbers
 str: Immutable strings
 tuple: Immutable tuples
 bool: Immutable booleans
 complex: Immutable complex numbers
 frozenset: Immutable sets

 Mutable Types in Python

 list: Mutable sequences of objects.
 dict: Mutable mappings of key-value pairs.
 set: Mutable collections of unique elements.
 bytearray: Mutable sequences of bytes.
 user-defined classes: Typically mutable, allowing for the creation of objects with custom behavior and state.

"""
import copy

# Original list with a mix of immutable (integer, string) and mutable (list) elements
original_list = [1, 'immutable string', [2, 3, 4]]

# Method 1: Using slicing
# This creates a new list by slicing the entire original list
copied_list_slicing = original_list[:]
print("Shallow Copy using slicing:", copied_list_slicing)

# Method 2: Using the list constructor
# The list constructor creates a new list from the original list
copied_list_constructor = list(original_list)
print("Shallow Copy using list constructor:", copied_list_constructor)

# Method 3: Using the copy method of a list
# Most direct way to copy a list in modern Python versions
copied_list_copy_method = original_list.copy()
print("Shallow Copy using copy method:", copied_list_copy_method)

# Method 4: Using the copy module
# General-purpose copy operation which defaults to a shallow copy
copied_list_copy_module = copy.copy(original_list)
print("Shallow Copy using copy module:", copied_list_copy_module)

# Method 5: Using list comprehension
# Creates a new list by iterating over and copying elements from the original list
copied_list_comprehension = [item for item in original_list]
print("Shallow Copy using list comprehension:", copied_list_comprehension)

# Method 6: Using the map function
# Applies a function (in this case, a lambda that returns the same value) to each element
copied_list_map = list(map(lambda x: x, original_list))
print("Shallow Copy using map function:", copied_list_map)

copied_list_copy_module = copy.copy(original_list)

copied_list_comprehension = [item for item in original_list]
copied_list_map = list(map(lambda x: x, original_list))

# Print the original and copied lists before modification
print("Original List:", original_list)
print("Shallow Copy using slicing:", copied_list_slicing)
print("Shallow Copy using list constructor:", copied_list_constructor)
print("Shallow Copy using copy method:", copied_list_copy_method)
print("Shallow Copy using copy module:", copied_list_copy_module)
print("Shallow Copy using list comprehension:", copied_list_comprehension)
print("Shallow Copy using map function:", copied_list_map)

# Modifying an immutable element in the original list
original_list[1] = 'modified string'

# Modifying a mutable element in the original list
original_list[2][0] = 'changed'

# Printing the lists after modification
print("\nAfter modifying the original list:")
print("Original List:", original_list)
print("Shallow Copy using slicing:", copied_list_slicing)
print("Shallow Copy using list constructor:", copied_list_constructor)
print("Shallow Copy using copy method:", copied_list_copy_method)
print("Shallow Copy using copy module:", copied_list_copy_module)
print("Shallow Copy using list comprehension:", copied_list_comprehension)
print("Shallow Copy using map function:", copied_list_map)

# Comprehension using values modification creates a sort of deep copy
original_list = [1, 2, 3]
copied_list_comprehension = [item + 2 for item in original_list]

# Modifying the original list
original_list[0] = 10
copied_list_comprehension[0] = 1000

print("Original List:", original_list)  # [10, 2, 3]
print("Copied List:", copied_list_comprehension)  # [3, 4, 5]
