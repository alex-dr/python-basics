# Basic while loop
count = 0
while count < 5:
    print("Count:", count)
    count += 1

# [Image of flowchart for basic while loop]

# Using break to exit the loop
number = 1
while True:
    if number % 7 == 0 and number % 5 == 0:
        print("Found the number:", number)
        break
    number += 1

# [Image of flowchart for while loop with break]

# Using continue to skip to the next iteration
for i in range(10):
    if i % 2 == 0:
        continue  # Skip even numbers
    print(i)

# [Image of flowchart for while loop with continue]

# Nesting while loops
outer_count = 0
while outer_count < 3:
    inner_count = 0
    while inner_count < 2:
        print("Outer count:", outer_count, "Inner count:", inner_count)
        inner_count += 1
    outer_count += 1
