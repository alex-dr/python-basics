from replit import clear

from calculator_art import logo


def add(n1, n2):
    return n1 + n2


def subtract(n1, n2):
    return n1 - n2


def multiply(n1, n2):
    return n1 * n2


def divide(n1, n2):
    return n1 / n2


operations = {
    "+": add,
    "-": subtract,
    "*": multiply,
    "/": divide
}


def get_float_input(order):
    while True:
        try:
            return float(input(f"What's the {order} number?: "))
        except ValueError:
            print("Please enter a valid number (e.g. 1000)")


def pick_operation():
    while True:

        for symbol in operations:
            print(symbol)

        operation_symbol = input("Pick an operation: ")
        if operation_symbol in operations:
            return operation_symbol


def calculator():
    print(logo)

    num1 = get_float_input("first")
    should_continue = True

    while should_continue:
        operation_symbol = pick_operation()
        num2 = get_float_input("next")
        calculation_function = operations[operation_symbol]
        answer = calculation_function(num1, num2)
        print(f"{num1} {operation_symbol} {num2} = {answer}")

        if input(f"Type 'y' to continue calculating with {answer}, or type 'n' to start a new calculation: ") == 'y':
            num1 = answer
        else:
            should_continue = False
            clear()
            calculator()


calculator()
