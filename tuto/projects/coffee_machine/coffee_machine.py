MENU = {
    "espresso": {
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    "latte": {
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    "cappuccino": {
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    }
}

profit = 0
resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
}


def is_resource_sufficient(order_ingredients):
    """Returns True when order can be made, False if ingredients are insufficient."""
    for item in order_ingredients:
        if order_ingredients[item] > resources[item]:
            print(f"​Sorry there is not enough {item}.")
            return False
    return True


def get_coin_amount(coin_type):
    while True:
        try:
            coin_amount = int(input(f" {coin_type}: "))
        except ValueError:
            print("Please enter a valid number.")
            continue
        else:
            return coin_amount


def process_coins():
    """Returns the total calculated from coins inserted."""
    print("Please insert coins.")
    total = get_coin_amount("how many quarters?: ") * 0.25
    total += get_coin_amount("how many dimes?: ") * 0.1
    total += get_coin_amount("how many dimes?: ") * 0.1
    total += get_coin_amount("how many nickles?: ") * 0.05
    total += get_coin_amount("how many pennies?: ") * 0.01
    return total


def is_transaction_successful(money_received, drink_cost):
    """Return True when the payment is accepted, or False if money is insufficient."""
    if money_received >= drink_cost:
        change = round(money_received - drink_cost, 2)
        print(f"Here is ${change} in change.")
        global profit
        profit += drink_cost
        return True
    else:
        print("Sorry that's not enough money. Money refunded.")
        return False


def make_coffee(drink_name, order_ingredients):
    """Deduct the required ingredients from the resources."""
    for item in order_ingredients:
        resources[item] -= order_ingredients[item]
    print(f"Here is your {drink_name} ☕️. Enjoy!")


def get_coffee_choice():
    """Returns the coffee choice."""
    coffee_choice = input("​What would you like? (espresso/latte/cappuccino): ").lower()

    while coffee_choice not in MENU and coffee_choice != "off" and coffee_choice != "report":
        coffee_choice = input("​What would you like? (espresso/latte/cappuccino): ").lower()
    return coffee_choice


def print_report():
    print(f"Water: {resources['water']}ml")
    print(f"Milk: {resources['milk']}ml")
    print(f"Coffee: {resources['coffee']}g")
    print(f"Money: ${profit}")


is_on = True

while is_on:
    choice = get_coffee_choice()
    if choice == "off":
        is_on = False
        print("Turning off the coffee machine.")
    elif choice == "report":
        print_report()
    else:
        drink = MENU[choice]
        if is_resource_sufficient(drink["ingredients"]):
            payment = process_coins()
            if is_transaction_successful(payment, drink["cost"]):
                make_coffee(choice, drink["ingredients"])
