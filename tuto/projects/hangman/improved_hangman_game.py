import random
from hangman_words import word_list
from hangman_art import logo, stages


def play_hangman():
    chosen_word = random.choice(word_list)
    word_length = len(chosen_word)
    end_of_game = False
    lives = 6
    guessed_letters = []

    print(logo)

    display = ["_"] * word_length

    while not end_of_game:
        guess = input("Guess a letter: ").lower()
        # If the user has entered a letter they've already guessed, print the letter and let them know.
        if guess in guessed_letters:
            print(f"You've already guessed {guess}")
            continue
        guessed_letters.append(guess)

        if guess in chosen_word:
            print(f"You correctly guessed {guess}, {lives} more lives remaining.")
            for position in range(word_length):
                if chosen_word[position] == guess:
                    display[position] = guess

        else:
            # If the letter is not in the chosen_word, print out the letter and let them know it's not in the word.
            lives -= 1
            if lives == 0:
                end_of_game = True
                print("You lose.")
                print(f"The word was {chosen_word}")
                break
            else:
                print(f"You guessed {guess}, that's not in the word. You lose a life.\nRemaining lives: {lives}")

        print(f"{' '.join(display)}")
        print(stages[lives])

        if "_" not in display:
            end_of_game = True
            print("You win.")


if __name__ == "__main__":
    play_hangman()
