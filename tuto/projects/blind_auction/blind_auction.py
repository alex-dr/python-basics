from replit import clear
from blind_auction_art import logo

print(logo)

bids = {}
bidding_finished = False


def get_bid_value():
    while True:
        try:
            bid_value = int(input("What's your bid?: $"))
            return bid_value
        except ValueError:
            print("Please enter a valid integer number (e.g. 1000)")


def finish_biding():
    while True:
        continue_biding = input("Are there any other bidders? Type 'yes or 'no'.\n")
        if continue_biding.lower() == "no":
            return True
        elif continue_biding.lower() == "yes":
            return False
        else:
            print("Please enter 'yes' or 'no'")


def find_highest_bidder(bidding_record):
    highest_bid = 0
    winner = ""
    # bidding_record = {"Angela": 123, "James": 321}
    for bidder in bidding_record:
        bid_amount = bidding_record[bidder]
        if bid_amount > highest_bid:
            highest_bid = bid_amount
            winner = bidder
    print(f"The winner is {winner} with a bid of ${highest_bid}")


while not bidding_finished:
    name = input("What is your name?: ")
    price = get_bid_value()
    bids[name] = price
    bidding_finished = finish_biding()
    if bidding_finished:
        find_highest_bidder(bids)
    else:
        clear()

"""
FAQ: My console doesn't clear()

This will happen if you’re using an IDE other than replit. 
I’ll cover how to use PyCharm in Day 15. That said, you can write
 your own clear() function or configure your IDE like so: 

https://www.udemy.com/course/100-days-of-code/learn/lecture/19279420#questions/16084076

"""
