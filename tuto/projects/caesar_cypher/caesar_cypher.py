from caesar_chpher_art import logo

''''
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
'''

# Simplified alphabet list
alphabet = [chr(i) for i in range(97, 123)]  # List comprehension to create the alphabet


# Definition of the caesar function which handles the encoding and decoding of text
def caesar(start_text, shift_amount, cipher_direction):
    # Initialize a variable to store the result
    end_text = ""

    # If the direction is 'decode', invert the shift amount to reverse the cipher
    if cipher_direction == "decode":
        shift_amount *= -1  # Iterate over each character in the input text
    for char in start_text:
        # Check if the character is in the alphabet (handles only alphabetic characters)
        if char in alphabet:
            # Find the position of the character in the alphabet
            position = alphabet.index(char)
            # Calculate the new position by adding the shift amount
            new_position = position + shift_amount
            # Add the character at the new position to the end_text
            end_text += alphabet[new_position]
        else:
            # If the character is not in the alphabet (like spaces, punctuation), add it as is
            end_text += char

    # Print the final result of the encoding/decoding process
    print(f"Here's the {cipher_direction}d result: {end_text}")


print(logo)


def get_cipher_direction():
    """
    This function prompts the user to input the direction for the cipher (either 'encode' or 'decode').
    It will keep asking until a valid response is given.
    """
    while True:
        encode_decode = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n").lower()
        if encode_decode in ["encode", "decode"]:
            return encode_decode
        else:
            print("Invalid input. Please type 'encode' or 'decode'.")


def get_shift_amount():
    """
    This function prompts the user to input the shift amount for the cipher.
    It will keep asking until a valid response is given.
    """
    while True:
        try:
            code_shift = int(input("Type the shift number:\n"))
            return code_shift
        except ValueError:
            print("Please enter a valid integer.")


def restart_or_end():
    """
    This function prompts the user to input whether they want to restart the cipher program.
    It will keep asking until a valid response is given.
    """
    while True:
        restart_confirmation = input("Type 'yes' if you want to go again. Otherwise type 'no'.\n").lower()
        if restart_confirmation in ["yes", "no"]:
            return restart_confirmation
        else:
            print("Invalid input. Please type 'yes' or 'no.")


should_end = False

while not should_end:

    direction = get_cipher_direction()
    text = input("Type your message:\n").lower()
    shift = get_shift_amount()
    # The shift amount should be between 0 and 25
    shift = int(shift) % 26

    caesar(start_text=text, shift_amount=shift, cipher_direction=direction)

    restart = restart_or_end()

    if restart == "no":
        should_end = True
        print("Goodbye")
    else:
        print("Continuing...")
