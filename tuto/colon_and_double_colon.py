# Example demonstrating the use of ':' and '::' in slicing

# 1. Using ':' in slicing
numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

# Extracting a portion of the list
# Syntax: list[start:stop]
# This slices the list from index 2 to 5 (exclusive of 5)
sublist = numbers[2:5]
print("Sublist:", sublist)  # [2, 3, 4]

# 2. Using ':' without start or end
# Omitting start - defaults to beginning of the list
first_three = numbers[:3]
print("First three elements:", first_three)  # [0, 1, 2]

# Omitting end - defaults to end of the list
last_three = numbers[-3:]
print("Last three elements:", last_three)  # [7, 8, 9]

# 3. Using '::' for stepping
# Syntax: list[start:stop:step]
# This slices the list from start to stop with a step
every_other = numbers[::2]
print("Every other element:", every_other)  # [0, 2, 4, 6, 8]

# 4. Reversing a list or string using '::'
# A common use of '::' is to reverse a list or string by using a step of -1
reversed_numbers = numbers[::-1]
print("Reversed list:", reversed_numbers)  # [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]

# Similarly, it can be used to reverse a string
sample_string = "Hello World"
reversed_string = sample_string[::-1]
print("Reversed string:", reversed_string)  # 'dlroW olleH'
