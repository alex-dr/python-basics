# 1. Lists
# Ordered, mutable, allow duplicates
numbers = [1, 4, 2, 5, 3]
print("List:", numbers)

# Accessing elements
print("First element:", numbers[0])
print("Last element:", numbers[-1])

# Modifying elements
numbers[2] = 8
print("Modified list:", numbers)

# Adding elements
numbers.append(6)
numbers.insert(2, 7)
print("List with added elements:", numbers)

# Removing elements
numbers.remove(5)
popped_element = numbers.pop()
print("List after removals:", numbers)
print("Popped element:", popped_element)

# 2. Tuples
# Ordered, immutable (cannot be changed after creation)
coordinates = (10, 20)
print("Tuple:", coordinates)

# Accessing elements (same as lists)
print("X-coordinate:", coordinates[0])

# 3. Sets
# Unordered, unique elements, mutable
fruits = {"apple", "banana", "apple"}  # Duplicates are automatically removed
print("Set:", fruits)

# Adding elements
fruits.add("orange")
print("Set with added element:", fruits)

# Removing elements
fruits.remove("banana")
print("Set after removal:", fruits)

# 4. Dictionaries
# Unordered, key-value pairs, mutable
person = {"name": "Alice", "age": 30, "city": "New York"}
print("Dictionary:", person)

# Accessing values by keys
print("Name:", person["name"])

# Adding and modifying key-value pairs
person["email"] = "alice@example.com"
person["age"] = 31  # Updating existing value
print("Updated dictionary:", person)
