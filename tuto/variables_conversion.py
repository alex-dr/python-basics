# Explicit type conversions
number_string = "123"
integer_number = int(number_string)  # Convert string to integer
float_number = float(number_string)  # Convert string to float

print("Integer:", integer_number, type(integer_number))
print("Float:", float_number, type(float_number))

# Implicit type conversion
integer_number = 5
float_number = 3.14
sum_result = integer_number + float_number  # Implicitly converts integer to float

print("Sum (implicit conversion):", sum_result, type(sum_result))

# Handling potential exceptions
try:
    non_numeric_string = "hello"
    integer_from_string = int(non_numeric_string)  # Will raise ValueError
except ValueError:
    print("Invalid conversion: Cannot convert string 'hello' to integer.")

# Using type checks before conversion
if number_string.isdigit():
    safe_integer_conversion = int(number_string)
    print("Safe integer conversion:", safe_integer_conversion)
else:
    print("Cannot convert string to integer: Non-numeric characters present.")

# Conversion between numerical types
float_value = 3.14159
int_value = int(float_value)  # Truncates decimal part
print("Integer from float:", int_value)





# Example of type conversion in Python with exception handling

# Integer to Float Conversion
try:
    int_value = 5
    float_value = float(int_value)
    print(f'Integer to float conversion: {float_value}')
except ValueError as e:
    print(f'Error converting integer to float: {str(e)}')

# String to Integer Conversion
try:
    str_value = '123'
    int_value = int(str_value)
    print(f'String to integer conversion: {int_value}')
except ValueError as e:
    print(f'Error converting string to integer: {str(e)}')

# String to Float Conversion
try:
    str_value = '123.45'
    float_value = float(str_value)
    print(f'String to float conversion: {float_value}')
except ValueError as e:
    print(f'Error converting string to float: {str(e)}')

# Handling conversion when the format is incorrect
try:
    str_value = 'not a number'
    # This will raise a ValueError because the string does not contain a valid number
    int_value = int(str_value)
except ValueError as e:
    print(f'Error: {str(e)} - conversion from string to integer failed due to invalid format.')

# Converting between incompatible types might raise TypeError
try:
    list_value = [1, 2, 3]
    str_value = str(list_value)  # This works fine
    int_value = int(list_value)  # This will raise a TypeError
except TypeError as e:
    print(f'Error: {str(e)} - conversion from list to integer is not possible.')
