def analyze_number(num):
    # First level block: Function definition
    if num > 0:
        # Second level block: If condition
        if num % 2 == 0:
            # Third level block: Nested if condition
            print(f"{num} is a positive even number.")
        else:
            # Third level block: Nested else condition
            print(f"{num} is a positive odd number.")
    elif num < 0:
        # Second level block: Elif condition
        print(f"{num} is a negative number.")
    else:
        # Second level block: Else condition
        print("The number is zero.")


# Test the function with different values
analyze_number(10)
analyze_number(-5)
analyze_number(3)
analyze_number(0)
