# This is a simple Python script to demonstrate the definition and use of functions

# Defining a function
# Here, we define a function named 'greet' that takes one parameter 'name'
def greet(name):
    """
    This function greets the person whose name is passed as an argument.
    """
    # The function prints a greeting message
    return f"Hello, {name}!"


# Using the function
# Now, we will call the function 'greet' with the argument 'Alice'
greeting_message = greet('Alice')
print(greeting_message)  # This will print: Hello, Alice!


# Defining another function
# This function 'add' takes two parameters and returns their sum
def add(num1, num2):
    """
    This function returns the sum of two numbers.
    """
    return num1 + num2


# Using the 'add' function
# We call 'add' with 3 and 4 as arguments
result = add(3, 4)
print(f"The sum of 3 and 4 is {result}")  # This will print: The sum of 3 and 4 is 7


# Demonstrating a function with a default parameter
def greet_with_salutation(name, salutation="Hello"):
    """
    This function greets a person with a given salutation.
    If no salutation is provided, it defaults to "Hello".
    """
    return f"{salutation}, {name}!"


# Using the function with the default parameter
print(greet_with_salutation("Bob"))  # Outputs: Hello, Bob!

# Using the function with a different salutation
print(greet_with_salutation("Bob", "Hi"))  # Outputs: Hi, Bob!

# Function to describe a book
def describe_book(title, author, genre, publication_year):
    """
    This function prints out a description of a book.

    Parameters:
    title (str): Title of the book
    author (str): Author of the book
    genre (str): Genre of the book
    publication_year (int): Year of publication
    """
    description = (f"Book Title: {title}\n"
                   f"Author: {author}\n"
                   f"Genre: {genre}\n"
                   f"Publication Year: {publication_year}")
    print(description)

# Using the function with keyword arguments
describe_book(title="1984",
              author="George Orwell",
              genre="Dystopian",
              publication_year=1949)

# The advantage of keyword arguments is that you can change the order of arguments
describe_book(genre="Fantasy",
              publication_year=1954,
              author="J.R.R. Tolkien",
              title="The Lord of the Rings")



# Function to calculate the mean, median, and mode of a list of numbers
# Multiple return value using tuple unpacking
def calculate_statistics(numbers):
    """
    Calculate and return the mean, median, and mode of a list of numbers.

    Parameters:
    numbers (list): A list of numbers.

    Returns:
    tuple: A tuple containing the mean, median, and mode of the list.
    """
    mean = sum(numbers) / len(numbers)

    sorted_numbers = sorted(numbers)
    n = len(sorted_numbers)
    middle = n // 2
    if n % 2 == 0:
        median = (sorted_numbers[middle - 1] + sorted_numbers[middle]) / 2
    else:
        median = sorted_numbers[middle]

    mode = max(sorted_numbers, key=sorted_numbers.count)

    return mean, median, mode


# Example usage of the function
numbers = [1, 3, 3, 4, 5, 5, 5, 7, 9]
mean, median, mode = calculate_statistics(numbers)

print(f"Mean: {mean}, Median: {median}, Mode: {mode}")

