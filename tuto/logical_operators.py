# AND operator (and)
has_ticket = True
is_adult = True
can_enter = has_ticket and is_adult
print("Can enter:", can_enter)

# OR operator (or)
has_discount = False
is_member = True
gets_discount = has_discount or is_member
print("Gets discount:", gets_discount)

# NOT operator (not)
is_raining = True
is_not_raining = not is_raining
print("Is not raining:", is_not_raining)

# Combining logical operators
temperature = 25
feels_good = (15 <= temperature <= 27) and not is_raining
print("Weather feels good:", feels_good)

# Using logical operators in conditional statements
if has_ticket and is_adult:
    print("Welcome to the show!")
else:
    print("You either need a ticket or to be an adult to enter.")

# Truthiness and falsiness
name = ""  # Empty string is considered false
if not name:
    print("Please enter your name.")
