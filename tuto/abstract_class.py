from abc import ABC, abstractmethod


# importing the ABC module from the abc package makes the class abstract
class Vehicle(ABC):
    def __init__(self, make, model):
        self.make = make
        self.model = model

    # The @abstractmethod decorator is used to define abstract methods
    @abstractmethod
    def display_vehicle_info(self):
        """ Abstract method, defined by convention only """
        pass

    @abstractmethod
    def start_engine(self):
        """ Abstract method to start the engine """
        pass


# Subclass of Vehicle
class Car(Vehicle):
    def display_vehicle_info(self):
        print(f"This is a {self.make} {self.model} car.")

    def start_engine(self):
        print("Starting the car's engine.")


# Subclass of Vehicle
class Motorcycle(Vehicle):
    def display_vehicle_info(self):
        print(f"This is a {self.make} {self.model} motorcycle.")

    def start_engine(self):
        print("Starting the motorcycle's engine.")


# Example Usage
car = Car("Toyota", "Corolla")
car.display_vehicle_info()
car.start_engine()

motorcycle = Motorcycle("Harley-Davidson", "Street 750")
motorcycle.display_vehicle_info()
motorcycle.start_engine()

print ("car is a vehicle ? ", isinstance(car, Vehicle) , "\n and its type is ", type(car))

# you can't create an instance of an abstract class because it's not fully implemented
