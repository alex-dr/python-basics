def dictionary_operations():
    # Creating a dictionary
    my_dict = {'a': 1, 'b': 2, 'c': 3}
    print("Original dictionary:", my_dict)

    # Adding a new key-value pair
    my_dict['d'] = 4
    print("Dictionary after adding a new item:", my_dict)

    # Updating an existing key's value
    my_dict['a'] = 10
    print("Dictionary after updating value:", my_dict)

    # Accessing a value with a key
    value_b = my_dict['b']
    print("Value associated with key 'b':", value_b)

    # Using get() method to access a value (safe access)
    value_e = my_dict.get('e', 'default_value_returned_if_key_not_found')
    print("Value for non-existing key 'e' with default:", value_e)

    # Removing a key-value pair using pop()
    removed_value = my_dict.pop('b', 'No key found')
    print("Value removed:", removed_value)
    print("Dictionary after pop:", my_dict)

    # Iterating over dictionary keys
    print("Iterating over keys:")
    for key in my_dict:
        print(key)

    # Iterating over dictionary values
    print("Iterating over values:")
    for value in my_dict.values():
        print(value)

    # Iterating over dictionary items (key-value pairs)
    print("Iterating over items:")
    for key, value in my_dict.items():
        print(f"{key}: {value}")

    # Checking if a key is in the dictionary
    if 'c' in my_dict:
        print("'c' is a key in the dictionary.")

    # Length of a dictionary
    print("Length of dictionary:", len(my_dict))

    # Using dict comprehension to create a new dictionary
    squared_dict = {k: v**2 for k, v in my_dict.items()}
    print("Dictionary with squared values:", squared_dict)

if __name__ == "__main__":
    dictionary_operations()
