import copy

# Original list with a mix of immutable (integer, string) and mutable (list) elements
original_list = [1, 'immutable string', [2, 3, 4]]

# Creating a deep copy of the original list
deep_copied_list = copy.deepcopy(original_list)

# Print the original and deep copied lists before modification
print("Original List:", original_list)
print("Deep Copied List:", deep_copied_list)

# Modifying an immutable element in the original list
original_list[1] = 'modified string'

# Modifying a mutable element in the original list
original_list[2][0] = 'changed'

# Printing the lists after modification
print("\nAfter modifying the original list:")
print("Original List:", original_list)  # The original list shows the changes
print("Deep Copied List:", deep_copied_list)  # The deep copied list remains unchanged

# Demonstrating structural changes
# Adding an element to the original list
original_list.append('new element')

# Printing the lists after adding a new element to the original list
print("\nAfter adding a new element to the original list:")
print("Original List:", original_list)  # The original list includes the new element
print("Deep Copied List:", deep_copied_list)  # The deep copied list does not include the new element
# The deep copied list is not affected by structural changes to the original list.
