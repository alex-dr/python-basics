# 1. Unpacking iterables with *
numbers = [1, 2, 3, 4, 5]
first, *middle, last = numbers
print("First:", first)  # 1
print("Middle:", middle)  # [2, 3, 4]
print("Last:", last)  # 5


# 2. Using * for variable number of arguments in function definition
def sum_all(*args):
    return sum(args)


print("Sum:", sum_all(1, 2, 3, 4))  # 10

# 3. Using * to repeat sequences
repeated_list = [1, 2] * 3
print("Repeated List:", repeated_list)  # [1, 2, 1, 2, 1, 2]


# 4. Unpacking dictionary keys and values with **
def print_values(**kwargs):
    for key, value in kwargs.items():
        print(f"{key} = {value}")


data = {'a': 1, 'b': 2}
print_values(**data)  # a = 1, b = 2


# 5. Using ** for accepting arbitrary keyword arguments in function
def print_kwargs(**kwargs):
    for key, value in kwargs.items():
        print(f"{key}: {value}")


print_kwargs(firstname='John', lastname='Doe')  # firstname: John, lastname: Doe
