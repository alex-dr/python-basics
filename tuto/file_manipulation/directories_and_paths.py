import os
import tempfile
import shutil


# Function to create a directory
def create_directory(path):
    try:
        os.makedirs(path)
        print(f"Directory created at: {path}")
    except FileExistsError:
        print(f"Directory already exists: {path}")
    except OSError as error:
        print(f"Error creating directory: {error}")


# Function to create a temporary directory
def create_temp_directory():
    try:
        temp_dir = tempfile.mkdtemp()
        print(f"Temporary directory created at: {temp_dir}")
        return temp_dir
    except OSError as error:
        print(f"Error creating temporary directory: {error}")
        return None


# Function to handle different types of paths
def handle_paths():
    current_dir = os.getcwd()
    print(f"Current Directory: {current_dir}")

    # Constructing a file path
    file_path = os.path.join(current_dir, 'example.txt')
    print(f"File Path: {file_path}")

    # Getting the absolute path
    absolute_path = os.path.abspath('..')
    print(f"Parent Directory (Absolute Path): {absolute_path}")

    # Splitting a file path
    directory, filename = os.path.split(file_path)
    print(f"Directory: {directory}, Filename: {filename}")


# Example usage
if __name__ == "__main__":
    # Creating a directory
    create_directory("test_directory")

    # Creating a temporary directory
    temp_dir = create_temp_directory()

    # Handling different types of paths
    handle_paths()

    # Clean up: Delete the created directory and temporary directory
    if temp_dir and os.path.exists(temp_dir):
        shutil.rmtree(temp_dir)
        print(f"Temporary directory deleted: {temp_dir}")

    if os.path.exists("test_directory"):
        os.rmdir("test_directory")
        print("Test directory deleted.")
