import os
import tempfile

# Define the filename
filename = 'example.txt'

# Write to a file
with open(filename, 'w') as file:
    file.write('Hello, World!')
    file.write('Hello, World!')
print(f"Created and wrote to {filename}")

# Read the file
if os.path.exists(filename):
    with open(filename, 'r') as file:
        content = file.read()
    print(f"Contents of {filename}: {content}")
else:
    print(f"{filename} does not exist")

# Opening a file with UTF-8 encoding
with open(filename, 'r', encoding='utf-8') as file:
    content = file.read()
    print(content)

    # Reading a large file line by line
with open(filename, 'r') as file:
    for line in file:
        # Process each line
        print(line.strip())

# Append to the file
with open(filename, 'a') as file:
    file.write('\nMore text.')
print(f"Appended to {filename}")

# Read the file again to show the append operation
with open(filename, 'r') as file:
    content = file.read()
print(f"Contents of {filename} after appending: {content}")

# Rename the file
new_filename = 'example_new.txt'
os.rename(filename, new_filename)
print(f"Renamed {filename} to {new_filename}")

# Delete the file
if os.path.exists(new_filename):
    os.remove(new_filename)
    print(f"Deleted {new_filename}")
else:
    print(f"{new_filename} does not exist, so it cannot be deleted")

# Final operation description
print("Final file operation was deleting the file.")

filename = "nonexistent_file.txt"

try:
    os.remove(filename)
except FileNotFoundError:
    print(f"Error: {filename} does not exist and cannot be deleted.")

# Creating a temporary file that is automatically deleted
with tempfile.TemporaryFile('w+t') as temp:
    # Write to the temporary file
    temp.write("Hello World!\n")
    temp.write("This is a temporary file.")

    # Go back to the beginning and read the file
    temp.seek(0)
    print(temp.read())
