
# Commonly Used Python Methods

# String Methods
sample_string = " Hello, World! "

# upper() - Converts a string into upper case
print(sample_string.upper())

# lower() - Converts a string into lower case
print(sample_string.lower())

# strip() - Removes any whitespace from the beginning or the end
print(sample_string.strip())

# split() - Splits the string into a list where each word is a list item
print(sample_string.split(','))

# join() - Joins the elements of an iterable to the end of the string
print('-'.join(['Hello', 'World']))

# find() - Searches the string for a specified value and returns the position
print(sample_string.find('World'))

# replace() - Returns a string where a specified value is replaced
print(sample_string.replace('Hello', 'Hi'))

# List Methods
sample_list = [1, 2, 3, 4, 5]

# append() - Appends an element to the end of the list
sample_list.append(6)
print(sample_list)

# extend() - Add the elements of a list to the end of the current list
sample_list.extend([7, 8])
print(sample_list)

# pop() - Removes the element at the specified position
sample_list.pop()
print(sample_list)

# remove() - Removes the item with the specified value
sample_list.remove(2)
print(sample_list)

# sort() - Sorts the list
sample_list.sort()
print(sample_list)

# reverse() - Reverses the order of the list
sample_list.reverse()
print(sample_list)

# index() - Returns the index of the first element with the specified value
print(sample_list.index(3))

# count() - Returns the number of elements with the specified value
print(sample_list.count(3))

# Dictionary Methods
sample_dict = {'name': 'John', 'age': 30, 'city': 'New York'}

# get() - Returns the value of the specified key
print(sample_dict.get('name'))

# keys() - Returns a list containing the dictionary's keys
print(sample_dict.keys())

# values() - Returns a list of all the values in the dictionary
print(sample_dict.values())

# items() - Returns a list containing a tuple for each key-value pair
print(sample_dict.items())

# update() - Updates the dictionary with the specified key-value pairs
sample_dict.update({'age': 31})
print(sample_dict)

# pop() - Removes the element with the specified key
sample_dict.pop('city')
print(sample_dict)

# clear() - Removes all the elements from the dictionary
sample_dict.clear()
print(sample_dict)